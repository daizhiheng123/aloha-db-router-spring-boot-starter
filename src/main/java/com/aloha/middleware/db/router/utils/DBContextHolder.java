package com.aloha.middleware.db.router.utils;

/**
 * @author DaiZhiHeng
 * @description 用于获取当前线程中分库分表的信息
 * @date 2023/7/8 22:16
 */
public class DBContextHolder {

    /**
     * 保存数据库的信息
     */
    private static final ThreadLocal<String> dbKey = new ThreadLocal<>();

    /**
     * 保存数据库表的信息
     */
    private static final ThreadLocal<String> tbKey = new ThreadLocal<>();


    public static void setDBKey(String dbKeyIdx) {
        dbKey.set(dbKeyIdx);
    }

    public static String getDBKey() {
        return dbKey.get();
    }

    public static void setTBKey(String tbKeyIdx) {
        tbKey.set(tbKeyIdx);
    }

    public static String getTBKey() {
        return tbKey.get();
    }

    public static void clearDBKey() {
        dbKey.remove();
    }

    public static void clearTBKey() {
        tbKey.remove();
    }
}
