package com.aloha.middleware.db.router.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author DaiZhiHeng
 * @description 配置属性类
 * @date 2023/7/9 9:40
 */
@ConfigurationProperties(prefix = "aloha.db-router.config")
public class DBRouterProperties {

    // 分库的总数
    private String dbCount;

    // 每个分库的表的总数
    private String tbCount;

    // 默认数据库
    private String defaultName;

    // 分库分表的路由字段
    private String routerKey;

    // 每个库的具体配置
    private List<DataSourceDefinition> datasource = new ArrayList<>();

    public Map<String, DataSourceDefinition> getDatasource() {
        return datasource.stream().collect(Collectors.toMap(DataSourceDefinition::getName, definition -> definition));
    }

    public static class DataSourceDefinition {
        private String name;
        private String driverClassName;
        private String url;
        private String username;
        private String password;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDriverClassName() {
            return driverClassName;
        }

        public void setDriverClassName(String driverClassName) {
            this.driverClassName = driverClassName;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }
    }

    public String getDbCount() {
        return dbCount;
    }

    public void setDbCount(String dbCount) {
        this.dbCount = dbCount;
    }

    public String getTbCount() {
        return tbCount;
    }

    public void setTbCount(String tbCount) {
        this.tbCount = tbCount;
    }

    public String getDefaultName() {
        return defaultName;
    }

    public void setDefaultName(String defaultName) {
        this.defaultName = defaultName;
    }

    public String getRouterKey() {
        return routerKey;
    }

    public void setRouterKey(String routerKey) {
        this.routerKey = routerKey;
    }

    public void setDatasource(List<DataSourceDefinition> dataSourceDefinitions) {
        this.datasource = dataSourceDefinitions;
    }
}
