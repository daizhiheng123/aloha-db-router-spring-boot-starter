package com.aloha.middleware.db.router.annotation;

import java.lang.annotation.*;

/**
 * @author DaiZhiHeng
 * @description 分库的依据
 * @date 2023/7/8 22:09
 */

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD})
public @interface DBRouter {

    /**
     * key为表中某个字段，可以由该字段计算到所路由的数据库的索引
     */
    String key() default "";
}
