package com.aloha.middleware.db.router.annotation;

import java.lang.annotation.*;

/**
 * @author DaiZhiHeng
 * @description 是否分表
 * @date 2023/7/8 22:12
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD})
public @interface DBRouterStrategy {

    /**
     * 如果该属性为 true 表明需要分表
     */
    boolean splitTable() default false;

}
