## 0. 为什么分库分表

**分库**

+ 第一，当业务量上升，单机MySQL的磁盘容量不足
+ 第二，数据库的连接是有限的，在高并发的场景下，大量的请求访问数据库

**分表**

+ 当一张表中的数据量太大的话，SQL查询的效率会降低

## 1. 动态数据的实现

在SpringBoot中提供了一个  **`AbstractRoutingDataSource`** 的抽象类，我们可以通过继承这个类，并重写

**`determineCurrentLookupKey()`** 方法，在这个方法里定义切换数据源的逻辑。我们现在要做的就是提供默认数据源和动态切换的多数据源

在这个项目中，通过解析配置文件来创建数据源，并在配置类中将 **`AbstractRoutingDataSource`** 的实现类创建出来，并给它设置上默认的数据源和需要动态切换的数据源

![image-20230713222748654](./img/image-20230713222748654.png)

根据从配置文件中读取到的定义信息构造数据源的方法 **`com.aloha.middleware.db.router.config.DBRouterAutoConfiguration#buildDataSource()`**

![image-20230713223406569](./img/image-20230713223406569.png)



创建一个 **`AbstractRoutingDataSource`** 的实现类并将默认数据源和动态切换的数据源设置进去

**`com.aloha.middleware.db.router.config.DBRouterAutoConfiguration#dataSource()`**

![image-20230713223620221](./img/image-20230713223620221.png)



那么数据源是怎么切换的呢？看一下源码

![image-20230713224640014](./img/image-20230713224640014.png)





## 2. 使用

### **依赖**

```java
<dependencyManagement>
    <dependencies>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-dependencies</artifactId>
            <version>2.3.12.RELEASE</version>
            <scope>import</scope>
            <type>pom</type>
        </dependency>
    </dependencies>
</dependencyManagement>

<dependencies>
    <dependency>
        <groupId>junit</groupId>
        <artifactId>junit</artifactId>
        <scope>test</scope>
    </dependency>
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-test</artifactId>
        <scope>test</scope>
    </dependency>

    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-web</artifactId>
    </dependency>
    <dependency>
        <groupId>com.aloha</groupId>
        <artifactId>aloha-db-router-spring-boot-starter</artifactId>
        <version>1.0-SNAPSHOT</version>
    </dependency>
    <dependency>
        <groupId>org.mybatis.spring.boot</groupId>
        <artifactId>mybatis-spring-boot-starter</artifactId>
        <version>2.2.0</version>
    </dependency>
    <dependency>
        <groupId>mysql</groupId>
        <artifactId>mysql-connector-java</artifactId>
        <scope>runtime</scope>
    </dependency>
</dependencies>
```



### **配置**

![image-20230731200630621](./img/image-20230731200630621.png)



### 注解

![image-20230731200826565](./img/image-20230731200826565.png)

![image-20230731200857681](./img/image-20230731200857681.png)





